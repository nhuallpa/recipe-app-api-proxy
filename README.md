# recipe-app-api-proxy

Recipe app API proxy application

## Usage

### Enviroment Variables

* `LISTEN_PORT` - Port to listen on (default: `8080`)
* `APP_HOST` - Hostname of the app to forward request to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)